﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApiSample.DataAccess.Models;

namespace WebApiSample.Api.Pre21
{
    public class Utilities
    {
        #region SortByGroupAndSubGroup
        public List<Product> SortByGroupAndSubGroup(List<Product> products)
        {
            Boolean GroupIDNotOK = true;
            Product TempProduct = new Product();

            if (products.Count > 1)
            {
                while (GroupIDNotOK)
                {
                    GroupIDNotOK = false;

                    for (int i = 0; i < (products.Count - 1); i++)
                    {
                        if (products[i].GroupID > products[i + 1].GroupID)
                        {
                            TempProduct = products[i];
                            products[i] = products[i + 1];
                            products[i + 1] = TempProduct;
                            GroupIDNotOK = true;
                            break;
                        }
                        else
                        {
                            if ((products[i].GroupID == products[i + 1].GroupID)
                                && (products[i].SubGroupID > products[i + 1].SubGroupID))
                            {
                                TempProduct = products[i];
                                products[i] = products[i + 1];
                                products[i + 1] = TempProduct;
                                GroupIDNotOK = true;
                                break;
                            }

                        }

                    }
                }
            }

            return products;
        }
        #endregion

        #region ListNoContainID
        public Boolean ListNoContainID(List<int> inputList, int containInt)
        {
            Boolean retBool = true;
            foreach (int listInt in inputList)
            {
                if (containInt == listInt)
                {
                    retBool = false;
                    break;
                }

            }

            return retBool;
        }
        #endregion

        #region StrContainID
        public Boolean StrNoContainID(string inputString, int containInt)
        {
            Boolean retBool = true;
            string[] storeId;
            try
            {
                if (inputString.Contains(','))
                {
                    storeId = inputString.Split(',');
                    foreach (string storeIdStr in storeId)
                    {
                        int storeInt = int.Parse(storeIdStr);
                        if (containInt == storeInt)
                        {
                            retBool = false;
                            break;
                        }
                    }
                }
                else
                {
                    int storeInt = int.Parse(inputString);
                    if(containInt == storeInt)
                        retBool = false;
                }
            }
            catch
            {
                retBool = true;
            }
            

            return retBool;
        }
        #endregion
    }
}
