﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApiSample.DataAccess.Models;
using WebApiSample.DataAccess.Repositories;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiSample.Api.Pre21
{
    #region StoreControllerHead
    [Route("api/[controller]")]
    public class StoreController : Controller
    #endregion
    {
        private readonly ProductRepository _repository;
        private Utilities utilities = new Utilities();

        public StoreController(ProductRepository repository)
        {
            _repository = repository;
        }

        #region GetAsync()
        // GET: api/<controller>
        [HttpGet("{StoreId}")]
        public async Task<IEnumerable<string>> GetByIdAsync(int StoreId)
        {
            List<Product> products = null;

            products = await _repository.GetProductsAsync();

            products = utilities.SortByGroupAndSubGroup(products);

            List<string> returnStrings = new List<string>();


            StringBuilder GroupTabStr = new StringBuilder();

            List<int> GroupsIDs = new List<int>();


            foreach (Product product in products)
            {
                if (utilities.ListNoContainID(GroupsIDs, product.GroupID))
                {
                    GroupsIDs.Add(product.GroupID);
                }
            }

            List<int> storesList = new List<int>();
            string[] storeId;
            for (int GroupInt = 0; GroupInt < GroupsIDs.Count; GroupInt++)
            {
                GroupTabStr.Append("===");
                foreach (Product product in products)
                {
                    storesList.Clear();
                    if ((product.StoreIDs != null)
                        && (product.StoreIDs.Length > 0))
                    {
                        try
                        {
                            if (product.StoreIDs.Contains(','))
                            {
                                storeId = product.StoreIDs.Split(',');
                                foreach (string storeIdStr in storeId)
                                {
                                    int storeInt = int.Parse(storeIdStr);
                                    storesList.Add(storeInt);
                                }
                            }
                            else
                            {
                                int storeInt = int.Parse(product.StoreIDs);
                                storesList.Add(storeInt);
                            }
                        }
                        catch
                        {
                            storesList.Clear();
                        }
                    }

                    if ((product.GroupID == GroupsIDs[GroupInt])
                        && (!utilities.ListNoContainID(storesList, StoreId)))
                    {
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "StoreIDs=" + product.StoreIDs.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "GroupID=" + product.GroupID.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "SubGroupID=" + product.SubGroupID.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "ID=" + product.id.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "name=" + product.name.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "Description=" + product.Description.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "ProductAddDateTime=" + product.ProductAddDateTime.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "Price=" + product.Price.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "VATPrice=" + product.VATPrice.ToString());
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "VATRate=" + product.VATRate.ToString() + "%");
                        returnStrings.Add(GroupTabStr.ToString() + ">" + "*******************************************");
                    }
                }


            }

            return returnStrings;
        }
        #endregion





    }

}
