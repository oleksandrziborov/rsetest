﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiSample.Api.Pre21;

using WebApiSample.DataAccess.Models;
using WebApiSample.DataAccess.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


#region TreeControllerHead
[Route("api/[controller]")]
public class TreeController : ControllerBase
#endregion
{
    private readonly ProductRepository _repository;
    private Utilities utilities = new Utilities();

    public TreeController(ProductRepository repository)
    {
        _repository = repository;
    }

    #region GetAsync()
    // GET: api/<controller>
    [HttpGet]
    [ProducesResponseType(typeof(List<Product>), StatusCodes.Status200OK)]
    public async Task<IEnumerable<string>> GetAsync()
    {
        List<Product> products = null;

        products = await _repository.GetProductsAsync();

        
        products = utilities.SortByGroupAndSubGroup(products);



        List<string> returnStrings = new List<string>();


        StringBuilder GroupTabStr = new StringBuilder();
        
        List<int> GroupsIDs = new List<int>();
        

        foreach (Product product in products)
        {
            if (utilities.ListNoContainID(GroupsIDs, product.GroupID))
            {
                GroupsIDs.Add(product.GroupID);
            }          
        }

        for (int GroupInt = 0; GroupInt < GroupsIDs.Count; GroupInt++)
        {
            GroupTabStr.Append("===");
            foreach (Product product in products)
            {
                if (product.GroupID == GroupsIDs[GroupInt])
                {                                          
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "GroupID=" + product.GroupID.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "SubGroupID=" + product.SubGroupID.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "ID=" + product.id.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "name=" + product.name.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "Description=" + product.Description.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "StoreIDs=" + product.StoreIDs.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "ProductAddDateTime=" + product.ProductAddDateTime.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "Price=" + product.Price.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "VATPrice=" + product.VATPrice.ToString());
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "VATRate=" + product.VATRate.ToString() + "%");
                    returnStrings.Add(GroupTabStr.ToString() + ">" + "*******************************************");
                }
            }
            

        }
        
        return returnStrings;
    }
    #endregion

    



}
