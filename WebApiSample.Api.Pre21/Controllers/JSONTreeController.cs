﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiSample.Api.Pre21;

using WebApiSample.DataAccess.Models;
using WebApiSample.DataAccess.Repositories;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiSample.Api.Pre21.Controllers
{
    #region JSONTreeControllerHead
    [Route("api/[controller]")]
    public class JSONTreeController : Controller
    #endregion
    {
        private readonly ProductRepository _repository;
        private Utilities utilities = new Utilities();

        public JSONTreeController(ProductRepository repository)
        {
            _repository = repository;
        }

        #region GetAsync()
        // GET: api/<controller>
        [HttpGet]
        [ProducesResponseType(typeof(Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAsync()
        {
            List<Product> products = null;

            products = await _repository.GetProductsAsync();

            products = utilities.SortByGroupAndSubGroup(products);

            return Ok(products);
        }
        #endregion
    }
}
