﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using WebApiSample.DataAccess.Models;
using WebApiSample.DataAccess.Repositories;
using System;

namespace WebApiSample.DataAccess.Repositories
{
    #region snippet_ControllerSignature
    [Route("api/[controller]")]
    public class ProductsController : ControllerBase
    #endregion
    {
        private readonly ProductRepository _repository;

        public ProductsController(ProductRepository repository)
        {
            _repository = repository;
        }

        #region snippet_GetById
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByIdAsync(int id)
        {
            var product = await _repository.GetProductAsync(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }
        #endregion


        #region snippet_BindingSourceAttributes
        [HttpGet]
        [ProducesResponseType(typeof(List<Product>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetAsync()
        {
            List<Product> products = null;

            products = await _repository.GetProductsAsync();

            return Ok(products);
        }
        #endregion

        [HttpPost]
        [ProducesResponseType(typeof(Product), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateAsync([FromBody] Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (product.GroupID == 0)
                return BadRequest("Error! GroupID cannot be 0!");

            product.id = 0;

            product.ProductAddDateTime = DateTime.Now;

            if ((product.Price > 0.0)
                && (product.VATRate >= 0.0))
                product.VATPrice = product.Price 
                    + (product.Price / 100.0) * product.VATRate;

            await _repository.AddProductAsync(product);

            return CreatedAtAction(nameof(GetByIdAsync),
                new { id = product.id }, product);
        }

        /*// DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Product), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public void Delete(int id)
        {
            
        }*/
    }
}