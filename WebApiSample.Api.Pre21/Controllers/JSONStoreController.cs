﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApiSample.DataAccess.Models;
using WebApiSample.DataAccess.Repositories;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApiSample.Api.Pre21.Controllers
{
    #region JSONStoreControllerHead
    [Route("api/[controller]")]
    public class JSONStoreController : Controller
    #endregion
    {
        private readonly ProductRepository _repository;
        private Utilities utilities = new Utilities();

        public JSONStoreController(ProductRepository repository)
        {
            _repository = repository;
        }

        #region GetAsync()
        // GET: api/<controller>
        [HttpGet("{StoreId}")]
        [ProducesResponseType(typeof(Product), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetByIdAsync(int StoreId)
        {
            List<Product> products = null;

            products = await _repository.GetProductsAsync();

            Product product = new Product();

            int i = 0;
            while (i < products.Count)
            {
                product = products[i];
                if (utilities.StrNoContainID(product.StoreIDs, StoreId))
                {
                    products.Remove(product);
                }
                else
                {
                    i++;
                }
            }
     
            
            products = utilities.SortByGroupAndSubGroup(products);

            return Ok(products);
        }
        #endregion
    }
}
