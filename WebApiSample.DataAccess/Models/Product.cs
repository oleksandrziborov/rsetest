﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebApiSample.DataAccess.Models
{
    /// <summary>
    /// Product 
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Product's ID. If == 0 will be set in future
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// Products group ID. If GroupID == 0 - no group. (Products without a group are not supported)
        /// </summary>
        [Required]
        public int GroupID { get; set; }
        /// <summary>
        /// Product group / subgroup ID If SubGroupID == 0 - no group.
        /// </summary>
        public int SubGroupID { get; set; }
        [Required]
        public string name { get; set; }

        public string Description { get; set; }

        
        /// <summary>
        /// Store's information is about which product can be sold in which store and get a list of products sold in this store
        /// </summary>
        public string StoreIDs { get; set; }
        /// <summary>
        /// When product was added
        /// </summary>
        public DateTime ProductAddDateTime { get; set; }
        /// <summary>
        /// Product price
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Product price with VAT
        /// </summary>
        public double VATPrice { get; set; }
        /// <summary>
        /// VAT rate (%)
        /// </summary>
        public double VATRate { get; set; }
    }
    
}
