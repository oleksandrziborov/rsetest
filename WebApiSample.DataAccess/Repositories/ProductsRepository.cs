﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApiSample.DataAccess.Models;
using System.Globalization;
using System;

namespace WebApiSample.DataAccess.Repositories
{
    public class ProductRepository
    {
        private readonly ProductContext _context;

        public ProductRepository(ProductContext context)
        {
            _context = context;

            if (_context.Products.Count() == 0)
            {
                _context.Products.AddRange(
                    new Product
                    {
                        name = "Book8",
                        Description = "Book5 description",
                        GroupID = 15,
                        SubGroupID = 7,
                        StoreIDs = "4,5,6",
                        ProductAddDateTime = DateTime.Now,
                        Price = 44.4,
                        VATRate = 20,
                        VATPrice = (44.4 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Book7",
                        Description = "Book4 description",
                        GroupID = 15,
                        SubGroupID = 5,
                        StoreIDs = "1,2,3",
                        ProductAddDateTime = DateTime.Now,
                        Price = 55.5,
                        VATRate = 20,
                        VATPrice = (55.5 / 100.0) * 20
                    },                 
                    new Product
                    {
                        name = "Book9",
                        Description = "Book6 description",
                        GroupID = 6,
                        SubGroupID = 8,
                        StoreIDs = "7,8,9",
                        ProductAddDateTime = DateTime.Now,
                        Price = 33.3,
                        VATRate = 20,
                        VATPrice = (33.3 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Learning ASP.NET Core",
                        Description = "A best-selling book covering the fundamentals of ASP.NET Core",
                        GroupID = 6,
                        SubGroupID = 4,
                        StoreIDs = "1,2,3",
                        ProductAddDateTime = DateTime.Now,
                        Price = 55.5,
                        VATRate = 20,
                        VATPrice = (55.5 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Learning EF Core",
                        Description = "A best-selling book covering the fundamentals of Entity Framework Core",
                        GroupID = 20,
                        SubGroupID = 21,
                        StoreIDs = "4,5,6",
                        ProductAddDateTime = DateTime.Now,
                        Price = 44.4,
                        VATRate = 20,
                        VATPrice = (44.4 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Book3",
                        Description = "Book3 description",
                        GroupID = 13,
                        SubGroupID = 33,
                        StoreIDs = "7,8,9",
                        ProductAddDateTime = DateTime.Now,
                        Price = 33.3,
                        VATRate = 20,
                        VATPrice = (33.3 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Book4",
                        Description = "Book4 description",
                        GroupID = 13,
                        SubGroupID = 1,
                        StoreIDs = "1,2,3",
                        ProductAddDateTime = DateTime.Now,
                        Price = 55.5,
                        VATRate = 20,
                        VATPrice = (55.5 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Book5",
                        Description = "Book5 description",
                        GroupID = 13,
                        SubGroupID = 11,
                        StoreIDs = "4,5,6",
                        ProductAddDateTime = DateTime.Now,
                        Price = 44.4,
                        VATRate = 20,
                        VATPrice = (44.4 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Book6",
                        Description = "Book6 description",
                        GroupID = 32,
                        SubGroupID = 18,
                        StoreIDs = "7,8,9",
                        ProductAddDateTime = DateTime.Now,
                        Price = 33.3,
                        VATRate = 20,
                        VATPrice = (33.3 / 100.0) * 20
                    },
                    new Product
                    {
                        name = "Book7",
                        Description = "Book7 description",
                        GroupID = 32,
                        SubGroupID = 19,
                        StoreIDs = "7,8,9",
                        ProductAddDateTime = DateTime.Now,
                        Price = 66.3,
                        VATRate = 20,
                        VATPrice = (66.3 / 100.0) * 20
                    }

                    );
                _context.SaveChanges();
            }
        }

        public async Task<List<Product>> GetProductsAsync()
        {
            return await _context.Products.ToListAsync();
        }

        public async Task<Product> GetProductAsync(int id)
        {
            return await _context.Products.FindAsync(id);
        }

        public async Task<int> AddProductAsync(Product product)
        {
            int rowsAffected = 0;

            _context.Products.Add(product);
            rowsAffected = await _context.SaveChangesAsync();

            return rowsAffected;
        }
    }
}
